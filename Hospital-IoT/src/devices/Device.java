package devices;

import java.io.PrintWriter;
import java.util.HashMap;

public class Device {
    
    private String idDevice;
    private int idx = 0;
    private HashMap<Integer,IODevice> mappa;
    
    public Device(String idDevice){
        this.idDevice = idDevice;
        mappa = new HashMap<Integer, IODevice>();
    }
    
    public void addDevice(IODevice iodevice){
        idx++;
        mappa.put(idx, iodevice);
        iodevice.setIDX(idx);    
    }
    
    public void interpreta(String readLine, PrintWriter out) {
        String[] split = readLine.split(" ");
        
        if("GET".equals(split[0])){
            String idx_string = split[1];
            int parseInt = Integer.parseInt(idx_string);
            
            IODevice get = mappa.get(parseInt);
            
            if(get != null){
               String value = get.getValueString();
               out.write("OK " + idx_string + " " + value + "\n");
            }
        } else if("SET".equals(split[0])){
            String idx_string= split[1];
            int parseInt = Integer.parseInt(idx_string);
             
            IODevice get = mappa.get(parseInt);
            
            if(get!=null){
                String value = split[2];
                get.parserAndSetValue(value);
                out.write("OK " + idx_string + " " + value + "\n");
            }
        } else {
          out.write("ERROR 1" + readLine + "\n"); //ERROR 1 command unknown
        }
    }
    
}
