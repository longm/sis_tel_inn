package devices;

public abstract class IODevice<typeOfValue>  {
    
    private int IDX;
    private String name;
    private typeOfValue value;
    private IODeviceListener listener;

    public IODevice(String name) {
        this.name = name;
    }

    public int getIDX() {
        return IDX;
    }

    public void setIDX(int IDX) {
        this.IDX = IDX;
        CommitListener();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public typeOfValue getValue() {
        return value;
    }

    public void setValue(typeOfValue value) {
        this.value = value;
        CommitListener();
    }
    
    public IODeviceListener getListener() {
        return listener;
    }

    public void setListener(IODeviceListener listener) {
        this.listener = listener;
    }
     
    public String getValueString() {
        return String.valueOf(value);
    }
        
    public abstract void parserAndSetValue(String valueString);
    
    public void CommitListener(){
         if(listener!=null){
            listener.onChangeDeviceValue();
         }
    }
    
    /* public Integer randomValue(Integer to, Integer from) {
    	int random = (int )((Math.random()*((to-from)+1))+from);
    	return random;
    } */
        
}
