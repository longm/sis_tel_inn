package devices;

public interface IODeviceListener {
    
    public void onChangeDeviceValue();
    
}
