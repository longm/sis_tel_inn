package iotsimulator;

import devices.AnalogDevice;
import devices.Device;
import devices.DigitDevice;
import devices.IODevice;
import driver.TCPServerDriver;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout;
import javax.swing.LayoutStyle.ComponentPlacement;

public class Simulator extends javax.swing.JFrame {

    public Device MyDevice;
    public static Simulator CInstance;

    // Components Graphic
    private javax.swing.JInternalFrame jInternalFrame1;
    private javax.swing.JInternalFrame jInternalFrame2;
    private iotsimulator.AnalogValue analogValue1;
    private iotsimulator.AnalogValue analogValue2;
    private iotsimulator.AnalogValue analogValue3;
    private iotsimulator.DigitValue digitValue1;
    private iotsimulator.DigitValue digitValue2;
    private iotsimulator.DigitValue digitValue3;
    private iotsimulator.DigitValue digitValue4;
    
    public Simulator() {
        initComponents();
        CInstance = this;
        Runnable task = new Runnable() {
        
            @Override
            public void run() {
            	
                MyDevice = new Device("Hospital Room");
            	
                // Sensori Analogici
                IODevice dev = new AnalogDevice("Patient Temperature");
                MyDevice.addDevice(dev);
                analogValue1.setDevice(dev);
                
                dev = new AnalogDevice("Room Temperature");
                MyDevice.addDevice(dev);
                analogValue2.setDevice(dev);
                
                dev = new AnalogDevice("Heartbeat");
                MyDevice.addDevice(dev);
                analogValue3.setDevice(dev);
                
                // Sensori Digitali
                dev = new DigitDevice("Door");
                MyDevice.addDevice(dev);
                digitValue1.setDevice((DigitDevice) dev);
                
                dev = new DigitDevice("Window");
                MyDevice.addDevice(dev);
                digitValue2.setDevice((DigitDevice) dev);
                
                dev = new DigitDevice("Air Conditioner");
                MyDevice.addDevice(dev);
                digitValue3.setDevice((DigitDevice) dev);
                
                dev = new DigitDevice("Alarm");
                MyDevice.addDevice(dev);
                digitValue4.setDevice((DigitDevice) dev);
                
                TCPServerDriver tp = new TCPServerDriver(MyDevice);
                tp.Start();
            }
        };
        Thread thread = new Thread(task);
        thread.start();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     */
    @SuppressWarnings("unchecked")
    private void initComponents() {

        jInternalFrame1 = new javax.swing.JInternalFrame();
        jInternalFrame2 = new javax.swing.JInternalFrame();
        analogValue1 = new iotsimulator.AnalogValue();
        analogValue2 = new iotsimulator.AnalogValue();
        analogValue3 = new iotsimulator.AnalogValue();
        digitValue1 = new iotsimulator.DigitValue();
        digitValue2 = new iotsimulator.DigitValue();
        digitValue3 = new iotsimulator.DigitValue();
        digitValue4 = new iotsimulator.DigitValue();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jInternalFrame1.setTitle("Analogical Devices");
        jInternalFrame1.setToolTipText("");
        jInternalFrame1.setVisible(true);
        
        javax.swing.GroupLayout jInternalFrame1Layout = new javax.swing.GroupLayout(jInternalFrame1.getContentPane());
        jInternalFrame1Layout.setHorizontalGroup(
        	jInternalFrame1Layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(jInternalFrame1Layout.createSequentialGroup()
        			.addContainerGap()
        			.addGroup(jInternalFrame1Layout.createParallelGroup(Alignment.LEADING)
        				.addComponent(analogValue2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        				.addComponent(analogValue1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        				.addComponent(analogValue3, GroupLayout.DEFAULT_SIZE, 781, Short.MAX_VALUE))
        			.addContainerGap())
        );
        jInternalFrame1Layout.setVerticalGroup(
        	jInternalFrame1Layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(jInternalFrame1Layout.createSequentialGroup()
        			.addGap(24)
        			.addComponent(analogValue1, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
        			.addGap(18)
        			.addComponent(analogValue2, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
        			.addGap(26)
        			.addComponent(analogValue3, GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)
        			.addContainerGap(76, Short.MAX_VALUE))
        );
        jInternalFrame1.getContentPane().setLayout(jInternalFrame1Layout);

        jInternalFrame2.setTitle("Digital Devices");
        jInternalFrame2.setPreferredSize(new java.awt.Dimension(310, 310));
        jInternalFrame2.setVisible(true);
        
        javax.swing.GroupLayout jInternalFrame2Layout = new javax.swing.GroupLayout(jInternalFrame2.getContentPane());
        jInternalFrame2Layout.setHorizontalGroup(
        	jInternalFrame2Layout.createParallelGroup(Alignment.TRAILING)
        		.addGroup(jInternalFrame2Layout.createSequentialGroup()
        			.addContainerGap()
        			.addGroup(jInternalFrame2Layout.createParallelGroup(Alignment.LEADING)
        				.addComponent(digitValue3, GroupLayout.DEFAULT_SIZE, 781, Short.MAX_VALUE)
        				.addComponent(digitValue2, GroupLayout.DEFAULT_SIZE, 781, Short.MAX_VALUE)
        				.addComponent(digitValue1, GroupLayout.DEFAULT_SIZE, 781, Short.MAX_VALUE)
        				.addComponent(digitValue4, GroupLayout.PREFERRED_SIZE, 781, GroupLayout.PREFERRED_SIZE))
        			.addContainerGap())
        );
        jInternalFrame2Layout.setVerticalGroup(
        	jInternalFrame2Layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(jInternalFrame2Layout.createSequentialGroup()
        			.addGap(25)
        			.addComponent(digitValue1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        			.addGap(18)
        			.addComponent(digitValue2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        			.addPreferredGap(ComponentPlacement.UNRELATED)
        			.addComponent(digitValue3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        			.addGap(18)
        			.addComponent(digitValue4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        			.addContainerGap(51, Short.MAX_VALUE))
        );
        jInternalFrame2.getContentPane().setLayout(jInternalFrame2Layout);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        layout.setHorizontalGroup(
        	layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(layout.createSequentialGroup()
        			.addContainerGap()
        			.addGroup(layout.createParallelGroup(Alignment.TRAILING)
        				.addComponent(jInternalFrame2, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 817, Short.MAX_VALUE)
        				.addComponent(jInternalFrame1, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 817, GroupLayout.PREFERRED_SIZE))
        			.addContainerGap(344, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
        	layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(layout.createSequentialGroup()
        			.addComponent(jInternalFrame1, GroupLayout.PREFERRED_SIZE, 397, GroupLayout.PREFERRED_SIZE)
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addComponent(jInternalFrame2, GroupLayout.PREFERRED_SIZE, 429, GroupLayout.PREFERRED_SIZE)
        			.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().setLayout(layout);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) {
        
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Simulator.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Simulator.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Simulator.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Simulator.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Simulator().setVisible(true);
            }
        });
    }
}
