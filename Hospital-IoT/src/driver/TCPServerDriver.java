package driver;

import devices.Device;
import java.net.ServerSocket;
import java.net.Socket;

public class TCPServerDriver {
    
    Device device;

    public TCPServerDriver(Device dev) {
        device = dev;
    }
    
    public void Start() {
        try {
            @SuppressWarnings("resource")
			ServerSocket serverSocket = new ServerSocket(9090);
            while (true) {
                Socket clientSocket = serverSocket.accept();
                TCPDriverInstance instance = new TCPDriverInstance(clientSocket, device);
                Thread thread = new Thread(instance);
                thread.start();
            }
        } catch (Exception ex) {
            //TODO logs
        }

    }
}
